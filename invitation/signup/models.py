from django.db import models, transaction


# Create your models here.


class Namelist(models.Model):
    image = models.ImageField(upload_to='', blank=True, null=True)
    nick_name = models.CharField('昵称', max_length=256, blank=True, null=True)
    phone_number = models.DecimalField('手机号', max_digits=11, decimal_places=0, blank=True, null=True)
    is_bring = models.BooleanField('是否带家属', default=False)
    family_member_num = models.IntegerField('携带家属人数', default=0)
    wechat_num = models.CharField('微信号', max_length=256, default='')
    is_attend = models.BooleanField('是否参加聚会', default=False)
    text = models.CharField('text', max_length=256, default='')

    @classmethod
    def apply(cls, nick_name, phone_number, is_bring, family_member_num, wechat_num):
        with transaction.atomic():
            sign_info = cls.objects.create(nick_name=nick_name, phone_number=phone_number, is_bring=is_bring,
                                           family_member_num=family_member_num, wechat_num=wechat_num)
        return sign_info

    def to_dict(self):
        result = {
            'nick_name': self.nick_name,
            'phone_number': self.phone_number,
            'is_bring': self.is_bring,
            'family_member_num': self.family_member_num,
            'wechat_num': self.wechat_num,
        }
        return result