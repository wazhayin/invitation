import json

from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views import View
from signup.models import Namelist


class User_Info(View):

    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        datas = request.POST
        nick_name = datas['nick_name']
        phone_number = datas['phone_number']
        is_bring = datas.get('is_bring')
        if is_bring:
            is_bring = True
        else:
            is_bring = False
        family_member_num = datas['family_member_num']
        wechat_num = datas['wechat_num']
        sign_info = Namelist.apply(nick_name, phone_number, is_bring, family_member_num, wechat_num)
        result = sign_info.to_dict()
        return render(result, 'apply.html')

