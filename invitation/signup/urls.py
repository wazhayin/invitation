from django.urls import path

from signup import views

urlpatterns = [
    path('info/', views.User_Info.as_view()),
]
